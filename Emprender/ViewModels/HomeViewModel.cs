﻿using Emprender.Models;
using Emprender.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Emprender.ViewModels
{
    public class HomeViewModel : BindableObject
    {
        public HomeViewModel(){

           LoadData();

        }
        private ObservableCollection<Modulo> _modulos;

        public ObservableCollection<Modulo> Modulos
        {
            get { return _modulos; }
            set
            {
                _modulos = value;
                OnPropertyChanged();
            }
        
        }
        private void LoadData()
        {
            Modulos = new ObservableCollection<Modulo>( ModuloService.Instance.GetModulos());
        }

    }
   
}
