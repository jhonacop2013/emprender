﻿using Emprender.Models;
using Emprender.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Emprender.ViewModels
{
    class PerfilViewModel : BindableObject
    {
        public PerfilViewModel()
        {

            LoadPerfiles();

        }
        private ObservableCollection<Perfil> _perfil;

        public ObservableCollection<Perfil> Perfiles
        {
            get { return _perfil; }
            set
            {
                _perfil = value;
                OnPropertyChanged();
            }

        }
        private void LoadPerfiles()
        {
            Perfiles = new ObservableCollection<Perfil>(UserService.Instance.GetPerfiles());
        }

    }
}