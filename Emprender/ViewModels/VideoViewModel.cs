﻿using Emprender.Models;
using Emprender.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Emprender.ViewModels
{
    class VideoViewModel : BindableObject
    {
        public VideoViewModel()
        {

            LoadVideos();

        }
        private ObservableCollection<Video> _videos ;

        public ObservableCollection<Video> Videos
        {
            get { return _videos; }
            set
            {
                _videos = value;
                OnPropertyChanged();
            }

        }
        private void LoadVideos()
        {
            Videos = new ObservableCollection<Video>(VideoService.Instance.GetVideos());
        }

    }
}