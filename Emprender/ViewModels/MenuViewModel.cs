﻿using Emprender.Models;
using Emprender.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Emprender.ViewModels
{
    public class MenuViewModel : BindableObject
    {
        public MenuViewModel()
        {

            LoadMenu();

        }
        private ObservableCollection<MenuObject> _menu;

        public ObservableCollection<MenuObject> MenuItems
        {
            get { return _menu; }
            set
            {
                _menu = value;
                OnPropertyChanged();
            }

        }
        private void LoadMenu()
        {
            MenuItems = new ObservableCollection<MenuObject>(NavigationService.Instance.GetMenu());
        }

    }
}