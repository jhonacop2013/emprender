﻿using Emprender.Models;
using Emprender.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Emprender.ViewModels
{
    public class JuegoViewModel : BindableObject
    {
        public JuegoViewModel()
        {

            LoadQuestions();

        }
        private ObservableCollection<Preguntas> _preguntas;

        public ObservableCollection<Preguntas> Preguntas
        {
            get { return _preguntas; }
            set
            {
                _preguntas = value;
                OnPropertyChanged();
            }

        }
        private void LoadQuestions()
        {
            Preguntas = new ObservableCollection<Preguntas>(JuegoService.Instance.GetQuestions());
        }

    }

}
