﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Emprender.Models
{
    public class Preguntas
    {
        public string Pregunta { get; set; }
        public bool EsCorrecto { get; set; }
    }

}
