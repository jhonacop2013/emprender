﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Emprender.Models
{
    public class Modulo
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Imagen { get; set; }
        public int Puntuacion { get; set; }



    }
}
