﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Emprender.Models
{
    public enum MenuItemType
    {
        Home,
        Perfil
    }
    public class MenuObject
    {
          public MenuItemType Id { get; set; }
            public string Titulo { get; set; }
            public string Icono { get; set; }
          
        }
    }
