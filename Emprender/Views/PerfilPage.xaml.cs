﻿using Emprender.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Emprender.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilPage : ContentPage
    {
        public PerfilPage()
        {
            InitializeComponent();
            BindingContext = new PerfilViewModel();
            BindingContext = new HomeViewModel();

            NavigationPage.SetHasNavigationBar(this, false);
        }
        public void menu_clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MenuPage());

        }

    }

}