﻿using Emprender.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Emprender.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JuegoPage : ContentPage
    {
        public JuegoPage()
        {
            InitializeComponent(); BindingContext = new HomeViewModel();
            NavigationPage.SetHasNavigationBar(this, false);

        }
        public void menu_clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MenuPage());

        }
        public void Play(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PreguntasView());

        }
    }
}