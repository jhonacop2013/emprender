﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Emprender.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AcercaDe : ContentPage
    {
        public AcercaDe()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
        public void Home(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Home());

        }


    }
}