﻿using Emprender.Services;
using Emprender.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Emprender.Views;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Emprender.Models;

namespace Emprender.Views
{

    public partial class Home : ContentPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public Home()
        {
            InitializeComponent();
            BindingContext = new HomeViewModel();
            NavigationPage.SetHasNavigationBar(this,false);

        }
        public void menu_clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MenuPage());

        }

       
    }
}
