﻿using Emprender.Models;
using Emprender.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Emprender.Views
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
            BindingContext = new MenuViewModel();
          
            //NavigationPage.SetHasNavigationBar(this, false);
            var navigationPage = Application.Current.MainPage as NavigationPage;
            navigationPage.BarBackgroundColor = Color.FromHex("#4d0b70");

            ListViewMenu.ItemSelected += (sender, e) =>
            {
                try
                {
                    if (e.SelectedItem == null)
                    {
                        return;
                    }
                    else
                    {
                        var id = e.SelectedItemIndex;
                        NavigateFromMenu(id);
                    }

                }
                catch (Exception ex)
                {

                    DisplayAlert("Error", ex.Message, "Ok");
                }
              



            };
        }
        public void OnButtonTapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.campusvirtualemprender.com/"));
        }


        public void NavigateFromMenu(int id)
        {
            if (id == 0)
            {
                Navigation.PushAsync(new Home());
            }
            else if (id == 1)
            {
                Navigation.PushAsync(new PerfilPage());
            }
            else if (id == 2)
            {
                Navigation.PushAsync(new ModulosInfoPage());
            }
            else if (id == 3)
            {
                Navigation.PushAsync(new JuegoPage());
            }
            else if (id == 4)
            {
                Navigation.PushAsync(new AcercaDe());
            }
       


        }
    }
}