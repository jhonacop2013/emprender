﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Emprender.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashScreen : ContentPage
    {
        Image logo;
        public SplashScreen()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            var layout = new AbsoluteLayout();


            logo = new Image
            {
                Source = "logo.png",
                WidthRequest = 200,
                HeightRequest = 200
            };

            AbsoluteLayout.SetLayoutFlags(logo, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(logo, new Rectangle (0.5,0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

            layout.Children.Add(logo);
            this.BackgroundColor = Color.FromHex("#431670");
            this.Content = layout;
            InitializeComponent();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await logo.FadeTo(1, 2000);
            Application.Current.MainPage = new NavigationPage(new Home());

        }
    }
}