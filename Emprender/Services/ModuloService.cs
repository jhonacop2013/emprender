﻿using Emprender.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Emprender.Services
{
    public class ModuloService
    {
        private static ModuloService _instance;

        public static ModuloService Instance {
            get {
                if (_instance == null)
                    _instance = new ModuloService();
                return _instance;
            }
        
        }
        public List<Modulo> GetModulos() {
            return new List<Modulo>
            {
                new Modulo { Nombre = "Proyecto de Vida", Descripcion="Comencemos por definir las metas y suños de nuestro plan de vida", Imagen ="single1.png", Puntuacion=1},
                new Modulo { Nombre = "Presupuesto", Descripcion = "Un presupuesto es una herramienta que nos permite organizar los gestos, totalizar los ingresos y ahorrar algode dinero para cumplir nuestras metas financieras", Imagen = "single2.png", Puntuacion = 1 },
                new Modulo { Nombre = "Acceso a servicios financieras", Descripcion="Conoce sobre los productos que nos ofrecen las entidades financieras", Imagen ="single3.png", Puntuacion=1},
                new Modulo { Nombre = "Ahorrar: ¡Si se puede!", Descripcion="Te invitamos a cumplir tus sueños por medio del ahorro, por que ahorrar ¡Si se puede!", Imagen ="single4.png", Puntuacion=1}

            };
            
        }
    }
}
