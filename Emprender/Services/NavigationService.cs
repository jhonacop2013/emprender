﻿using Emprender.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Emprender.Services
{
    public class NavigationService
    {
        private static NavigationService _instance;

        public static NavigationService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new NavigationService();
                return _instance;
            }

        }
        public List<MenuObject> GetMenu()
        {
            return new List<MenuObject>
            {
                new MenuObject { Titulo = "Inicio", Icono="casa.png" },
                new MenuObject { Titulo = "Perfil", Icono="usuario.png"},
                new MenuObject { Titulo = "Programas", Icono="file.png" },
                new MenuObject { Titulo = "Juegos", Icono="file.png"},
               new MenuObject { Titulo = "Acerca de" , Icono="casa.png"}



            };

        }
    }
}


